import React from 'react'
import { MDBFooter, MDBContainer, MDBRow, MDBCol, MDBIcon } from 'mdb-react-ui-kit';
import "../styles/MyFooter.css"
const Myfooter = () => {
  return (
    <div bgColor='light' className='text-center text-lg-start text-muted'>
      <section className='d-flex justify-content-center justify-content-lg-between p-4 border-bottom'>
        <div className='me-5 d-none d-lg-block'>
          <span>Get connected with us on social networks:</span>
        </div>

        <div>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="facebook-f" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="twitter" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="google" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="instagram" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="linkedin" />
          </a>
          <a href='' className='me-4 text-reset'>
            <MDBIcon fab icon="github" />
          </a>
        </div>
      </section>

      <section className=''>
        <MDBContainer className='text-center text-md-start mt-5'>
          <MDBRow className='mt-3'>
            <MDBCol md="3" lg="4" xl="3" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>
                <MDBIcon icon="gem" className="me-3" />
                <img className='w-50' src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQe0Ii5z5uTaOU7ezKyBDplGcPaz9Zlow797A&usqp=CAU" alt="" />
              </h6>
             
            </MDBCol>

            <MDBCol md="2" lg="2" xl="2" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Products</h6>
              <p>
                <a href='#!' className='text-reset'>
                  Dalmore
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  33 South
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Agavita
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  ABC
                </a>
              </p>
            </MDBCol>

            <MDBCol md="3" lg="2" xl="2" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Wine </h6>
              <p>
                <a href='#!' className='text-reset'>
                  Red Wine
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  White Wine
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Sauignon Blanc & Blends
                </a>
              </p>
              <p>
                <a href='#!' className='text-reset'>
                  Vodka
                </a>
              </p>
              <p>
              <a href='#!' className='text-reset'>
                  Whisky
                </a>
              </p>
              <p>
              <a href='#!' className='text-reset'>
                  Single malt Scotch Whisky
                </a>
              </p>
            </MDBCol>
            <MDBCol md="3" lg="2" xl="2" className='mx-auto mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Spirits </h6>
              
              <p>
                <a href='#!' className='text-reset'>
                  Vodka
                </a>
              </p>
              <p>
              <a href='#!' className='text-reset'>
                  Whisky
                </a>
              </p>
              <p>
              <a href='#!' className='text-reset'>
                  Single malt Scotch Whisky
                </a>
              </p>
            </MDBCol>


            <MDBCol md="4" lg="3" xl="3" className='mx-auto mb-md-0 mb-4'>
              <h6 className='text-uppercase fw-bold mb-4'>Contact</h6>
              <p>
                <MDBIcon icon="home" className="me-2 " />
                Address:#19-21 Eo, Street 240, Phnom Penh, Cambodia
              </p>
              <p>
                <MDBIcon icon="envelope" className="me-3" />
                Email:sme@redaproncambodia.com
              </p>
              <p>
                <MDBIcon icon="phone" className="me-3" /> Phone Number:023 981 888
              </p>
             
            </MDBCol>
          </MDBRow>
        </MDBContainer>
      </section>

      <div className='text-center p-4' style={{ backgroundColor: 'rgba(0, 0, 0, 0.05)' }}>
      © 2023 All Rights Reserved. Design by{" "}
        <a className='text-reset fw-bold' href='https://mdbootstrap.com/'>
          Sopha
        </a>
      </div>
    </div>
  )
}

export default Myfooter
