import React from 'react'
import "../styles/ServicePage.css"
const Service = () => {
  return (
    <>
    <div className='banner-img' style={{
      backgroundImage:`url(https://redapron.ca/wp-content/uploads/2021/05/JUL-25-18-0331.jpg)`,
      width:'100%', height:'500px', 
      }}>
        <div className="banner-healine">
        <span className=' bg-light '>about Red Apron</span>
        </div>
       
    </div>
    <div className="containerx">
      <div className="contained-narrow-left">
        <div className="regular_content px-2 pt-3">
          <p>We are residents of our communtity and activ participants in our local economy. Our team is committed to getting people 'back to the table' eating healthful, delicious food and our table includes you!</p>
          <p>Our menus change daily, weekly and seasonally using ingredients that are sourced locally when possible, and at their freshest. Our list of local farmers and producers is lengthy, and through these relationships your food dollars support hundreds of people in our community.</p>
          <p>We value the connection between food, human health, and the wellness of our community. How we spend our food dollars is an important statement about our priorities.</p>
          <p>Our clients appreciate our commitment to these values. Our service is well suited to clients who want to eat consciously, and who also lead busy lives. Our service offers the gift of time – time to spend with family and friends.</p>
        </div>
      </div>
    </div>
    <div className="container    mb-5">
        <div className='d-flex flex-column container flex-wrap flex-md-row justify-content-center gap-5 '>
           <div className="item-cardx px-4 pb-3 col-sm-12 col-md-4 col-lg-3 d-flex align-items-center justify-content-center ">
                       <h3 >Receive Our Weekly Menus</h3>
                       <a href="#" className='btnx mx-5'>Sign Up</a>
           </div>
           <div className="item-cardx px-4 pb-3 mt-4  col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid pt-3 "
                            src="https://redapron.ca/wp-content/themes/redapron21/images/red-apron-logo.svg"
                            alt=" cabernet Franc "
                        />
                        <p >© Copyright 2023 Red Apron. All Rights Reserved Worldwide. Please do not pilfer, re-write or otherwise annex our hard work. Thank you.</p>
                        
           </div>
        </div>
    </div>
    </>
  )
}

export default Service
