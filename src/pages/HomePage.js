import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faArrowRight, faPaperPlane } from "@fortawesome/free-solid-svg-icons";

import "../styles/HomePage.css"

const HomePage = () => {

    return (
        <div className="container">
            <div className="d-flex header-section flex-column flex-md-row  mt-4 ">
                <div className="container pt-4   order-2 order-md-1">
                    <h1 className="display-2 header-title">
                        {" "}
                        Welcome to Redapron Shop
                    </h1>
                    <p> We are a team of passionate people whose goal is to improve everyone's life through disruptive products. We build great products to solve your business problems.</p>
                    <div className="d-flex pt-5">
                        <button className="btn btn-success px-4 py-2">
                            
                            <strong> Go to Menu </strong>
                        </button>
                      
                    </div>
                </div>

                <div className="order-md-2 logo-img">
                    <img
                        className="img-fluid "
                        src="https://www.rollingstone.com/wp-content/uploads/2022/05/AdobeStock_164010848.jpeg"
                        alt="image logo "
                    />
                     
                </div>
            </div>

            <div className="container text-center mt-5 pt-5 mb-5">
                <h1 className="feature-title">
                    {" "}
                    The Grape of Wine
                    <span className="feature-title text-success pre-scrollable">
                        Red Wine and White Wine
                    </span>
                </h1>
                <div className="d-flex flex-column container flex-wrap flex-md-row justify-content-center gap-5 mt-5 ">
                    <div className="item-card px-4 pb-3  col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid pt-3"
                            src="https://media.istockphoto.com/id/846300870/photo/cabernet-franc-grapes-on-the-vine-ready-for-harvest.jpg?s=612x612&w=0&k=20&c=wQeT66sqXachsamwhm0FBtL6Wd1157fRvwm5qWxL5fg="
                            alt=" cabernet Franc "
                        />
                        <h2> Cabernet Franc</h2>
                        <p> Cabernet franc is a classic medium-bodied red with moderate tannins. The flavor of cabernet franc wines are defined by a balance between red fruits, herbs, and peppery earthiness. Cabernet franc has medium-to-high acidity that makes it refreshingly easy-to-drink.</p>
                    </div>
                    <div className="item-card px-4 pb-3  col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid pt-3"
                            src="https://media.istockphoto.com/id/846300870/photo/cabernet-franc-grapes-on-the-vine-ready-for-harvest.jpg?s=612x612&w=0&k=20&c=wQeT66sqXachsamwhm0FBtL6Wd1157fRvwm5qWxL5fg="
                            alt=" cabernet Franc "
                        />
                        <h2> Cabernet Franc</h2>
                        <p> Cabernet franc is a classic medium-bodied red with moderate tannins. The flavor of cabernet franc wines are defined by a balance between red fruits, herbs, and peppery earthiness. Cabernet franc has medium-to-high acidity that makes it refreshingly easy-to-drink.</p>
                    </div>
                    <div className="item-card px-4 pb-3 mt-3  col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid pt-3"
                            src="https://media.istockphoto.com/id/846300870/photo/cabernet-franc-grapes-on-the-vine-ready-for-harvest.jpg?s=612x612&w=0&k=20&c=wQeT66sqXachsamwhm0FBtL6Wd1157fRvwm5qWxL5fg="
                            alt=" cabernet Franc "
                        />
                        <h2> Cabernet Franc</h2>
                        <p> Cabernet franc is a classic medium-bodied red with moderate tannins. The flavor of cabernet franc wines are defined by a balance between red fruits, herbs, and peppery earthiness. Cabernet franc has medium-to-high acidity that makes it refreshingly easy-to-drink.</p>
                    </div>
                    <div className="item-card px-4 pb-3 mt-3 col-sm-12 col-md-4 col-lg-3">
                        <img
                            className="img-fluid pt-3"
                            src="https://media.istockphoto.com/id/531628593/photo/grape.jpg?s=612x612&w=0&k=20&c=cfoYUDddCM90cmPY54LRRaw6--S-u1U-B3kAfBa7cN0="
                            alt=" plant image "
                        />
                        <h2> Chardonnay</h2>
                        <p> Chardonnay depending on where it grows and how it's made. But typically, Chardonnay is a dry, medium- to full-bodied wine with moderate acidity and alcohol. Its flavors range from apple and lemon to papaya and pineapple, and it also shows notes of vanilla when it's aged with oak.</p>
                    </div>
                    
                </div>
            </div>

            <div
                className="container section3 d-flex flex-column 
      flex-md-row mt-5 pt-5 justify-content-center align-items-center"
            >
                <div className="image-side col-sm-12 col-md-6 col-lg-5 mt-5 ">
                    <img
                        className="img-fluid" style={{width:"20%"}}
                        src="https://cdn11.bigcommerce.com/s-oyi93ews/images/stencil/960w/products/16552/12320/esprit_de_pavie__57221.1659126327.jpg?c=2"
                        alt="image flower"
                    />
                </div>

                <div className="text-side ">
                    <h1 className="feature-title">Esprit De Pavie 2015 Bordeaux 750ml</h1>
                    <ul>
                        <li>This vintage is a blend of 65% Merlot, 20% Cabernet Franc and 15% Cabernet Sauvignon</li>
                        <li> Was aged for 15 months in one-year-old barrels. Deep garnet-purple colored, the 2015 Esprit de Pavie (a blend of vines in Côtes do Castillon and young vines from Pavie and Monbousquet) opens with expressive red and black plums notes plus touches of baking spices, vanilla pod, dark chocolate and dried Provence herbs. Medium to full-bodied, the bright fruit fills the mouth with plenty of spicy red and black fruits, supported by chewy tannins and finishing with a refreshing lift.</li>
                        
                    </ul>
                </div>
            </div>

            <div
                className="section4 mx-0 mx-lg-5 d-flex flex-column flex-md-row  justify-content-center mt-5       
      align-items-center 
      container"
            >
                <div className="text-side w-100 w-md-75  order-2 order-md-0">
                    <h1 className="feature-title">RIEDEL THE WINE GLASS COMPANY</h1>
                    <p className="w-100 w-lg-50 pe-0 pe-md-5">
                    
                    RIEDEL Crystal is a 300 year-old family owned company known for the creation and development of varietal-specific stemware. RIEDEL Crystal was the first in history to recognize that the taste and aroma of a beverage is affected by the shape of the vessel from which it is consumed, and has been recognized for its revolutionary designs complementing alcoholic beverages and other drinks.

                    </p>

                   
                </div>

                <div className="image-side">
                    <img
                        className="img-fluid rounded-3" style={{width:"80%"}}
                        src="https://img.riedel.com/ct/w_1840,q_100,hash_008b72/60911db7c15017946c3c-1e4ce00b1c6f4bf7f0f46bf77c747775.ssl.cf3.rackcdn.com/440000000_thumbnail-nWpUnaJX.jpg"
                        alt=" The Glass riedel"
                    />
                </div>
            </div>
        </div>
    );
};

export default HomePage;
